# BEAURIS - sample repo

This is an example repository using [BEAURIS](https://gitlab.com/beaur1s/beauris) to deploy genomic portals for sample data.

Have a look at [BEAURIS documentation](https://beauris.readthedocs.io/) for more information on setting up your own repo, following this example.
