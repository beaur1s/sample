---

# Example beauris.yml conf file

ansible:
  staging:
    host: 192.168.1.255  # Swarm controller ip
    envvars:  # Various ansible environment variables
      ANSIBLE_REMOTE_USER: beauris  # Remote ansible user. Ansible will ssh the controller using this user
      ANSIBLE_TASK_TIMEOUT: 600  # Make sure ansible does not hang
    extravars:  # Various ansible extra variables
      ansible_python_interpreter: /usr/bin/python3  # If not using python2 on the host
    custom_templates: ./ansible/templates/ # A directory containing templates overriding the ones in https://gitlab.com/beaur1s/beauris/-/tree/main/beauris/workflows/ansible/templates (not all files are required, path relative to beauris.yml)
  production:
    host: 192.168.2.255
    envvars:
      ANSIBLE_REMOTE_USER: beauris
      ANSIBLE_TASK_TIMEOUT: 600
    extravars:
      ansible_python_interpreter: /usr/bin/python3
    custom_templates: ./ansible/templates/

deploy:
  deploy_interface: True
  servers:
    staging:
      base_url: "https://staging.example.org/"  # Domain of deployed web apps
      url_prefix: "/sp/"  # URL prefix of deployed web apps (can be /)
      base_url_restricted: "https://staging.example.org/"  # Domain of deployed web apps (restricted access)
      url_prefix_restricted: "/sp_priv/"  # URL prefix of deployed web apps (restricted access, can be /)
      target_dir: "$DEPLOY_DIR/staging/"  # Where docker-compose.yml and mounted data volumes will be written
      services:  # List of default services enabled for each organism (can be overriden in each organism file)
        - blast
        - download
        - jbrowse
        - authelia
        - apollo
        - genoboo
        - elasticsearch
      append_staging: False  # If you want to append "_staging" to url prefixes when deploying in staging mode
      options:  # Various service specific options
        blast_theme: /some/path/to/custom/blast-themes/
        blast_job_dir: /projects/beaurisample/blast_jobs/
        authelia_conf: /projects/beaurisample/base_services/traefik/authelia/acl_beauris.yml
        authelia_conf_merge_with: /projects/beaurisample/base_services/traefik/authelia/acl_base.yml
        authelia_conf_merge_to: /projects/beaurisample/base_services/traefik/authelia/acl.yml
        authelia_service: traefik_authelia  # Name of the authelia Docker service
        extra_ref_data_dirs:
          - /some/other/ref_data_dir/  # When using data from an external directory
    production:
      base_url: "https://example.org/"
      url_prefix: "/sp/"
      base_url_restricted: "https://example.org/"
      url_prefix_restricted: "/sp_priv/"
      services:
        - blast
        - download
        - jbrowse
        - authelia
        - apollo
        - genoboo
        - elasticsearch
      target_dir: "$DEPLOY_DIR/prod/"
      options:
        blast_theme: /some/path/to/custom/blast-themes/
        blast_job_dir: /projects/beaurisample/blast_jobs/
        authelia_conf: /projects/beaurisample/base_services/traefik/authelia/acl_beauris.yml
        authelia_conf_merge_with: /projects/beaurisample/base_services/traefik/authelia/acl_base.yml
        authelia_conf_merge_to: /projects/beaurisample/base_services/traefik/authelia/acl.yml
        authelia_service: traefik_authelia  # Name of the authelia Docker service
        extra_ref_data_dirs:
          - /some/other/ref_data_dir/  # When using data from an external directory

data_locker:
  method: dir
  options:
    target_dir: "$LOCKED_DIR/"
    base_pattern: "{genus}/{species}/{strain}/"  # Pattern to build the path of files deposited in locked dir (based on entity metadata)
    pattern_input: "{type}/{version}/{filename}"  # Pattern to build the path of input files deposited in locked dir (based on entity metadata)
    pattern_derived: "{name}/{tool_version}-{date}-{revision}/{filename}"  # Pattern to build the path of derived files deposited in locked dir (based on entity metadata)
    locked_yml_dir: "$CI_PROJECT_DIR/locked/"  # Where locked yml will be commited in the Git repo
    locked_yml_dir_future: "$CI_PROJECT_DIR/future_locked/"  # Temp directory used internally by locking mechanism (not commited to Git)

# data_locker:
#   method: gopublish  # Not implemented (yet)
#   options:
#     url: 'https://example.org'
#     username: 'admin'
#     apikey: '$GOPUBLISH_APIKEY'
#     proxy_username: 'admin'
#     proxy_password: 'admin'

apollo:  # Connection infos for Apollo server
  staging:
    url: http://staging.apollo.example.org/apollo-staging/
    external_url: http://staging.apollo.example.org/apollo-staging/
    user: "fakeuser"
    password: "$APOLLO_PASS_STAGING"
    public_group: "annotators"  # The public group in which all Apollo users are automatically added by ldapollo
  production:
    url: "http://apollo.example.org/apollo/"
    external_url: "https://apollo.example.org/apollo/"
    user: "fakeuser"
    password: "$APOLLO_PASS_PROD"
    public_group: "annotators"

picture_files: ./organism_pictures  # Git directory where organism pictures can be placed
on_job_fail: rerun  # "rerun" if you want BEAURIS to relaunch previously failed jobs

job_specs:  # Job specific options / env / Slurm settings
  drmaa:
    gffread:
      env: ". /local/env/envgffread-0.12.7.sh; . /local/env/envpython-3.9.5.sh"  # Example of env modification injected at the begining of the gffread task
      # For slurm (at least) mem must be in Mb without unit
      native_specification: "--mem=20000"  # Example of Slurm/DRMAA native specification
    func_annot_bipaa:
      env: ". /local/env/envpython-3.9.5.sh; export LD_LIBRARY_PATH=/data1/slurm/drmaa/lib/:$LD_LIBRARY_PATH; export DRMAA_LIBRARY_PATH=/data1/slurm/drmaa/lib/libdrmaa.so"
    fatotwobit:
      env: ". /local/env/envucsc-357.sh"
    blastdb:
      env: ". /local/env/envblast-2.6.0.sh"
      native_specification: "--mem=20000"
    index_bai:
      env: ". /local/env/envsamtools-1.15.sh"
    bam_to_wig:
      env: ". /local/env/envucsc-357.sh && . /local/env/envbedtools-2.27.1.sh && . /local/env/envsamtools-1.15.sh"
      native_specification: "--cpus-per-task=1 --mem=8000"
  # Make sure to use single quotes and escape characters for re_protein
  galaxy:
    build_genoboo:  # Example of options passed by default to a task (can be overriden)
      re_protein: '\$1-P\$2'
      re_protein_capture: "^(.*?)-R([A-Z]+)$"
      blast_algorithm: "blastp"
      blast_matrix: "blosum62"
  local:
    build_elasticsearch:
      re_protein: '\1-P\2'
      re_protein_capture: "^(.*?)-R([A-Z]+)$"
      goterms_file: '/somewhere/go-basic.obo'
      to_index:
        - interpro
        - eggnog
        - diamond

tasks:  # List of per entity tasks that are enabled on this BEAURIS repo
  assembly:
    - fasta_check
    - jbrowse
    - fatotwobit
    - blastdb_assembly
    - apollo
    - apollo_perms
    - deploy_jbrowse
  annotation:
    - ogs_check
    - gffread
    - func_annot_bipaa
    - blastdb_cds
    - blastdb_proteins
    - blastdb_transcripts
  organism:
    - 'deploy_authelia'
    - 'deploy_download'
    - 'deploy_blast'
    - 'deploy_jbrowse'
    - 'build_genoboo'
    - 'deploy_genoboo'
    - 'build_elasticsearch'
    - 'deploy_elasticsearch'
  proteome:
    - blastdb_proteome
  transcriptome:
    - blastdb_transcriptome
  track:
    - track_check
    - index_bai
    - bam_to_wig
