# GitLab Runner for BEAURIS

## Declare the new GitLab Runner in GitLab UI

Go to `Settings` > `CI/CD` > `Runners`. Click on `New project runner`.

* Operating systems: `Linux`
* Tags: `beaurisample` (or any tag you like)
* Run untagged jobs: check
* Runner description: `A GitLab Runner running inside the GenOuest infrastructure, for ATLASea jobs`
* Lock to current projects: check
* Maximum job timeout: 172800 (=2 days)

Click on `Create runner`.

Ignore all the commands given in the following pages, just take note of the `runner authentication token` for the next step.

Disable the `Enable instance runners for this project` checkbox in `Settings` > `CI/CD` > `Runners` (to avoid using public runners from GitLab.com by default).

## Launching the runner

In production you will need to copy `config.toml.sample_beauris` to `config.toml`, and write the valid token in it (pasted from GitLab project settings).

Deploy from one of the Swarm cluster manager node, with:

```bash
docker stack deploy -c docker-compose.yml runner
```

After a small moment, the runner should appear as active in `Settings` > `CI/CD` > `Runners`
