# Upstream Nginx reverse proxy configuration

This is an example Nginx configuration, in case you want to have an Nginx reverse proxy between internet and Traefik.

On a typical Nginx server, you should place [beauris.conf](./beauris.conf) in `/etc/nginx/conf.d`, and place the following chunk of config in the `http {...}` block of `/etc/nginx/nginx.conf`:

```txt
    # load balancing for beauris traefik access
    upstream beauristraefikfrontssl {
        server swarmnode1.genouest.org:8889;
    }
```

Defining this upstream server once in `nginx.conf` makes it easier to change it quickly in case of maintenance on the server.
