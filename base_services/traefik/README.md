# Traefik and Authelia for BEAURIS

Traefik is a Docker container responsible for proxying the HTTP(S) trafic to each Docker container launched by BEAURIS.

Authelia is a Docker container responsible for authenticating users and giving authorization or not to access specific services deployed by BEAURIS.

Everytime you access a URL deployed by BEAURIS, your request will go through Traefik which:

* Will ask Authelia if you have the right to access it
* If yes, will forward your request to the corresponding Docker container running somewhere on your Swarm cluster.

In this example, we consider that Traefik is itself running behind a reverse proxy (Nginx for example).

## Configuring

The [provided authelia config file](./authelia/configuration.yml) is a slightly modified version of official Authelia's sample config.

It is pre-configured to use a Redis and a Postgres servers, as deployed in `docker-compose.yml`.

You are responsible for configuration the `authentication_backend` section (to use LDAP or file based accuont list). And you probably want to explore the various autentication methods available.

You *must* modify the Authelia configuration file, in particular update the secret keys:

* `identity_validation`>`reset_password`>`jwt_secret`
* `session`>`secret`
* `postgres`>`password`

Three additional configuration files are provided:

* `acl_base.yml`: base ACL that you want to define, regardless of BEAURIS deployed services
* `acl_beauris.yml`: BEAURIS will write in this file all the ACL he wants to define
* `acl.yml`: BEAURIS will merge both previous file to this one, inserting `acl_beauris.yml` content where `__BEAURIS_RULES__` is written in `acl_base.yml`

Authelia will only take into account the content of `acl.yml`. Having 3 different files allow more customisation of the order of rules.

You will also need to adapt a few lines in `docker-compose.yml`: look for `CHANGEME` keywords.

## Launching the runner

First set up secrets to store securely your LDAP password (fill it with any string if you don't want to enable LDAP connection)

```bash
mkdir secrets
echo "my_ldap_password" > secrets/LDAP_PASSWORD
chmod 700 -R secrets
chmod 600 secrets/*
```

Then deploy from one of the Swarm cluster manager node, with:

```bash
mkdir -p docker_data/authelia_db
mkdir -p docker_data/authelia_redis
docker stack deploy -c docker-compose.yml traefik
```
