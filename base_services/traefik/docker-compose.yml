version: '3.7'
services:
  traefik:
    image: traefik:2.11.0
    command:
      - "--api"
      - "--api.dashboard"
      - "--log.level=INFO"
      - "--global.sendAnonymousUsage=false"
      - "--global.checkNewVersion=false"
      - "--providers.docker=true"
      - "--providers.docker.swarmMode=true"
      - "--providers.docker.network=traefik"
      - "--entryPoints.web.address=:80"
      - "--entryPoints.web.forwardedHeaders.trustedIPs=192.168.1.11"  # CHANGEME IP address of the upstream reverse proxy (if any)
      - "--entryPoints.webs.address=:443"
      - "--entryPoints.webs.forwardedHeaders.trustedIPs=192.168.1.11"  # CHANGEME IP address of the upstream reverse proxy (if any)
      - "--serversTransport.insecureSkipVerify=true"
    ports:
      # In non-host mode (=8888:80 syntax), the 8888 port can be accessed on any swarm node
      # Forced to switch to host mode to allow getting a correct X-Forwarded-For header for Plausible (and possibly other apps)
      # The downside is that the upstream proxy needs to be configured to point to the swarm node where traefik is running
        # See https://dockerswarm.rocks/traefik/#getting-the-client-ip
      - target: 80
        published: 8888
        mode: host
      - target: 443
        published: 8889
        mode: host
    networks:
      - traefik
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    deploy:
      placement:
        constraints:
          - node.role == manager
          - "node.hostname==swarmnode1"  # CHANGEME replace with the hostname of one of the swarm node
      labels:
        - "traefik.http.routers.traefik-api.rule=PathPrefix(`/traefik`)"
        - "traefik.http.routers.traefik-api.tls=true"
        - "traefik.http.routers.traefik-api.entryPoints=webs"
        - "traefik.http.routers.traefik-api.service=api@internal"
        - "traefik.http.middlewares.traefik-strip.stripprefix.prefixes=/traefik"
        - "traefik.http.middlewares.traefik-auth.forwardauth.address=http://authelia:9091/api/authz/forward-auth"
        - "traefik.http.middlewares.traefik-auth.forwardauth.trustForwardHeader=true"
        - "traefik.http.routers.traefik-api.middlewares=traefik-auth,traefik-strip"
        # Dummy service for Swarm port detection. The port can be any valid integer value.
        - "traefik.http.services.traefik-svc.loadbalancer.server.port=9999"
        # Some generally useful middlewares for organisms hosting
        - "traefik.http.middlewares.sp-auth.forwardauth.address=http://authelia:9091/api/authz/forward-auth"
        - "traefik.http.middlewares.sp-auth.forwardauth.trustForwardHeader=true"
        - "traefik.http.middlewares.sp-auth.forwardauth.authResponseHeaders=Remote-User,Remote-Groups"
        - "traefik.http.middlewares.sp-trailslash.redirectregex.regex=^(https?://[^/]+/sp(_priv)?/[^/]+)$$"
        - "traefik.http.middlewares.sp-trailslash.redirectregex.replacement=$${1}/"
        - "traefik.http.middlewares.sp-trailslash.redirectregex.permanent=true"
        - "traefik.http.middlewares.sp-app-trailslash.redirectregex.regex=^(https?://[^/]+/sp(_priv)?/[^/]+/[^/]+)$$"
        - "traefik.http.middlewares.sp-app-trailslash.redirectregex.replacement=$${1}/"
        - "traefik.http.middlewares.sp-app-trailslash.redirectregex.permanent=true"
        - "traefik.http.middlewares.sp-blast-home.redirectregex.regex=^(https?://[^/]+/sp(_priv)?/[^/]+)/?$$"
        - "traefik.http.middlewares.sp-blast-home.redirectregex.replacement=$${1}/blast/"
        - "traefik.http.middlewares.sp-blast-home.redirectregex.permanent=true"
        - "traefik.http.middlewares.sp-prefix.stripprefixregex.regex=/sp(_priv)?/[^/]+"
        - "traefik.http.middlewares.sp-app-prefix.stripprefixregex.regex=/sp(_priv)?/[^/]+/[^/]+"
        - "traefik.http.middlewares.sp-big-req.buffering.maxRequestBodyBytes=50000000"
        - "traefik.http.middlewares.sp-huge-req.buffering.maxRequestBodyBytes=2000000000"
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s

  authelia:
    image: authelia/authelia:4.38.6
    networks:
      - traefik
    depends_on:
      - authelia-redis
      - authelia-db
    command:
      - "authelia"
      - "--config=/config/configuration.yml"
      - "--config=/config/acl.yml"
    volumes:
      - ./authelia/:/config/:ro
    secrets: [LDAP_PASSWORD]
    environment:
      AUTHELIA_AUTHENTICATION_BACKEND_LDAP_PASSWORD_FILE: /run/secrets/LDAP_PASSWORD
    deploy:
      labels:
        - "traefik.http.routers.authelia.rule=Host(`auth.beauris.example.org`)" # CHANGEME update url
        - "traefik.http.routers.authelia.tls=true"
        - "traefik.http.routers.authelia.entryPoints=webs"
        - "traefik.http.services.authelia.loadbalancer.server.port=9091"
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s

  authelia-redis:
    image: redis:7.2.4-alpine
    command: ["redis-server", "--appendonly", "yes"]
    volumes:
      - ./docker_data/authelia_redis/:/data/
    networks:
      - traefik
    deploy:
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s

  authelia-db:
    image: postgres:16.2-alpine
    environment:
      POSTGRES_PASSWORD: mypassword  # CHANGEME update this to match the one in authelia/configuration.yml
    volumes:
      - ./docker_data/authelia_db/:/var/lib/postgresql/data/
      - ../common/postgres-162-entrypoint.sh:/usr/local/bin/docker-entrypoint.sh
    networks:
        - traefik
    deploy:
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s

secrets:
  LDAP_PASSWORD:
    file: ./secrets/LDAP_PASSWORD

networks:
  traefik:
    driver: overlay
    name: traefik
    ipam:
      config:
        - subnet: 10.50.0.0/16
